#ifndef _PERC_PROGRAM_OPTIONS_H
#define _PERC_PROGRAM_OPTIONS_H

#include <boost/program_options.hpp>
#include <iostream>

namespace po = boost::program_options;

namespace percolation
{

namespace internal
{
void print_err(std::string parameter, const po::options_description& desc)
{

     std::cerr << "The ["<< parameter << "] parameter is missing!" << std::endl;
     std::cerr << desc << std::endl;
}

bool check_param(std::string parameter, const po::variables_map& vm)
{
    auto param_value = vm[parameter.c_str()].as<int>();
    assert(param_value > 0);
    if (param_value <= 0)
    {
        std::cerr << parameter << " value have to be > 0" << std::endl;
        return false;
    }

    return true;
}

}


/**
 * @brief ParseCommandLine
 * @param argc - number of command line arguments
 * @param argv - values of command line arguments list
 * @param k - size of the obstacle TODO: constraints
 * @param L - size of the system. Hint: try to use L = 2^k, limit = CPU mempory
 * @param d - dimmension of the system (2, 3, 4, ... , 7) the L^d system will be created
 * @param N - number of simulations
 * @param num_threads - number of threads to execute with
 * @return - return true if parsing was ok
 */
bool parseCommandLine(int argc, const char* argv[],
                      int& k, int& L,  int& d, int& N,
                      int& num_threads)
{
    po::options_description desc("Allowed options");

    desc.add_options()
        ("help,h", "Produce this message")
        ("obstacle_size,k", po::value(&k), "Size of the each obstacle in the system.")
        ("system_size,L", po::value(&L), "Size of the system.")
        ("dimmension,d", po::value(&d), "Dimmension of the system.")
        ("simulations,N", po::value(&N)->default_value(1000000),
            "Number of simulations performed for given parameters of the system. Default 1000000")
        ("num_threads,t", po::value(&num_threads)->default_value(1),
                                  "Number of threads, default num_threads = 1.");

    po::variables_map vm;

    try{
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    }
    catch(po::error& error)
    {
        std::cerr << "Parsing command line options..." << std::endl;
        std::cerr << "Error: " << error.what() << std::endl;
        std::cerr << "Use '--help' for command line options details." << std::endl;
        return false;
    }

    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        return false;
    }

    if(vm.count("obstacle_size") == 0)
    {
       internal::print_err("obstacle_size", desc);
       return false;
    }
    else
    {
        if (!internal::check_param("obstacle_size", vm))
            return false;
    }

    if(vm.count("system_size") == 0)
    {
       internal::print_err("system_size", desc);
       return false;
    }
    else
    {
        if (!internal::check_param("system_size", vm))
            return false;
    }


    if(vm.count("dimmension") == 0)
    {
       internal::print_err("dimmension", desc);
       return false;
    }
    else
    {
        auto dim = vm["dimmension"].as<int>();
        assert( dim < 8 && dim > 1);
        if (dim > 8 || dim < 2)
        {
            std::cerr << "Invalid dimmension value, d = 2, 3, 4, 5, 6 or 7";
            return false;
        }

    }


    std::cout << "Percolation params:"
              << " K = " << k
              << " L = "<< L
              << " DIM = " << d
              << " N = " << N
              << " t = " << num_threads << std::endl;

    return true;
}

} // namespace percolation


#endif //_PERC_PROGRAM_OPTIONS_H
