# README #

### What is this repository for? ###

* *Quick summary*   
The source codes for simulation and analysis of percolation of overlapping hypercubes in dimension 3 to 7, as described in the paper: "From discrete to continuous percolation in dimensions 3 to 7" by Z. Koza and J. Pola, University of Wrocław, Poland.

* *Version* 0.1

### How do I get set up? ###

The project uses cmake build tool minimal version 2.8. 

* Dependencies
    + boost
        - program options
        - random
        - system
        - filesystem
    + openmp
    + gcc-4.8 (C++ 11)

The code was developed and tested on Ubuntu 14.04 LTS.

* Compilation
    1. Clone repository using ```git clone https://bitbucket.org/ismk_uwr/percolation.git```.
    2. Go to ```percolation``` folder and create ```bin``` directory.
    3. Go to ```bin``` directory and call ```ccmake ../src```. The cmake configuration will now open.
    4. Press ```c``` to create initial configuration of the project
    5. Fill the ```CMAKE_BUILD_TYPE``` with desired value ```Release``` or ```Debug```
    6. Press ```c``` to configure project and then ```g``` to generate Makefile.
    7. Type ```make``` to build the project
 
### Running the simulations ###

Read the paper mentioned above. 

The project contains several source files:   

* `percol_wrap_dD.cpp` - the main program, which outputs `percolD?L*K*.dat` files containing the n_i numbers. The filename contains the values of D, L and K. The program works in append mode so that new simulation results can be readily appended to the existing ones.   
* `convert.cpp` - this program converts `percolD?L*K*.dat` into 10 `percolD?L*K*_[0-9].map` files that are compressed versions of the `*.dat` files, divided into 10 disjoint subsets to enable error analysis. By default, the output refers to case A. The program can also generate the `*.map` files for cases B and C in subdirectories `Umin` and `Umax`, respectively. 
* `histogtram3.cpp` - reads the \*.map files and performs the data conversion from microcanonical to canonical ensemble. Outputs the critical volume fractions in files `estimatorD?K*.txt` file in the append mode, and the slope of the probability distribution function at the critical volume fraction in files `y_pcD?K*.txt`, also in append mode. Can be compiled to work in extended, 80-bit floating-point mode. 
* `collect.cpp` - reads the *.txt files, computes the average values and their uncertainties, outputs `*.av` files with the critical volume fractions. Only columns 1, 4, and 5 are relevant for the publication.    

For a casual reader, two of these files might be of interest: `percol_wrap_dD.cpp`, which contains the union-find implementation of the wrapping percolation of overlapping hypercubes, and `histogram3.cpp`, which implements the microcanonical to canonical ensemble transformation. 

The code is distributed 'as is', without cleaning up any experimental, unused parts, diagnostic output, redundant output, accompanying scripts, etc. 

### Contribution guidelines ###
To contribute create a private fork. In your private fork make a new branch with proposed changes. After finishing create pull request to this repository.

### Who do I talk to? ###

* Repo owner or admin
    * Jakub Pola: jakub.pola2@uwr.edu.pl
    * Zbigniew Koza: zbigniew.koza@uwr.edu.pl


* Other community or team contact
    http://www.ift.uni.wroc.pl/~ismk/