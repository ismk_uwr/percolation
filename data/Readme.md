# DATA README #


### What is this directory for

This firectory contains the original files with the data for the `D`- and `k`-dependent percolation thresholds that we used in our paper. The data is divided into 5 main subdirectoriws, D3, D4,..., D7 holding the results for the space dimension d = 3,...,7. Each of them contains files ```estimatorD?K*.txt.av``` with the data for `D` and `K` encoded in the file name. 



### The structure of the files

Each file ```estimatorD?K*.txt.av``` contains five columns, which we will number 0,1,...,4. 

- Column 0: the value of `L`

- Column 1,2: we did not use them.

- Column 3: the finite-size estimator of the percolation threshold for `D`, `K` defined by the filename and `L` given in Column 0.

- Column 4: the error estimate of the value in Column 3, calculated as the std deviation from 10 data subsets. 


### Data for case A, B, C

The paper reports the results for cases A, B, and C.

- Case A: the data are stored in main subdirectories, that is, D3, D4,..., D7

- Case B: tha data are stored in subfolders `Umin`, that is, in D[3-7]/Umin

- Case C: tha data are stored in subfolders `Umax`, that is, in D[3-7]/Umax
   


### What next?

If you want to reproduce our results, you have to:

- extrapolate the data for given `D`, `K`, `L` to the limit of `L` \to infinity

- if you want to estimate the continuos percolation thresholds in D=3,...,7, you will also have to extrapolate the data for `K` \to infinity 
 

